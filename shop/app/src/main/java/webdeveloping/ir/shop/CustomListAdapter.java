package webdeveloping.ir.shop;

/**
 * Created by Administrator on 07/16/2017.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Locale;

public class CustomListAdapter extends ArrayAdapter<String> {

    private String[] articles={};
    private Context context;

    public CustomListAdapter(String[] articles, Context context) {
        super(context, R.layout.mylist, articles);
        this.articles = articles;
        this.context = context;

    }

    @Override
    public int getCount() {
        if (articles == null) {
            return 0;
        }
        return articles.length;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.mylist, parent, false);
        TextView catNameTextView = (TextView) view.findViewById(R.id.item);
        catNameTextView.setText(articles[position]);
        return view;
    }
}