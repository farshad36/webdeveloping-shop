package webdeveloping.ir.shop;

import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();

        if(intent.getBooleanExtra("first_login",false))
        {
            finish();
            Intent final_intent = new Intent(main.this, main.class);
            intent.putExtra("first_login",false);
            intent.putExtra("login_status",true);
            startActivity(intent);
        }
        //SharedPreferences
        SharedPreferences login_setting = getSharedPreferences("MyLoginSetting", MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = login_setting.edit();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(main.this, signup.class);
                startActivity(intent);
            }
        });
        SQLiteDatabase mydb;
        mydb = openOrCreateDatabase("shop", Context.MODE_PRIVATE, null);
        if (!isTableExists("article", true, mydb)) {
            mydb.execSQL("CREATE TABLE IF NOT EXISTS [article](id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT , price TEXT )");
            mydb.execSQL("insert into article (name,price) values ('1','1000000')");
            mydb.execSQL("insert into article (name,price) values ('2','700000')");
            mydb.execSQL("insert into article (name,price) values ('3','1000000')");
            mydb.execSQL("insert into article (name,price) values ('4','700000')");
            mydb.execSQL("insert into article (name,price) values ('5','1000000')");
            mydb.execSQL("insert into article (name,price) values ('6','700000')");
            mydb.execSQL("insert into article (name,price) values ('7','1000000')");
            mydb.execSQL("insert into article (name,price) values ('8','700000')");
            mydb.execSQL("insert into article (name,price) values ('9','1000000')");
            mydb.execSQL("insert into article (name,price) values ('10','700000')");
            mydb.execSQL("insert into article (name,price) values ('11','1000000')");
            mydb.execSQL("insert into article (name,price) values ('12','700000')");
            mydb.execSQL("insert into article (name,price) values ('13','1000000')");
            mydb.execSQL("insert into article (name,price) values ('14','700000')");
            mydb.execSQL("insert into article (name,price) values ('15','1000000')");
            mydb.execSQL("insert into article (name,price) values ('16','700000')");
            mydb.execSQL("insert into article (name,price) values ('17','1000000')");
            mydb.execSQL("insert into article (name,price) values ('18','700000')");
            mydb.execSQL("insert into article (name,price) values ('19','1000000')");
            mydb.execSQL("insert into article (name,price) values ('20','700000')");
            mydb.execSQL("insert into article (name,price) values ('21','1000000')");
            mydb.execSQL("insert into article (name,price) values ('22','700000')");
            mydb.execSQL("insert into article (name,price) values ('23','1000000')");
            mydb.execSQL("insert into article (name,price) values ('24','700000')");
            mydb.execSQL("insert into article (name,price) values ('25','1000000')");
            mydb.execSQL("insert into article (name,price) values ('26','700000')");
            mydb.execSQL("insert into article (name,price) values ('27','1000000')");
            mydb.execSQL("insert into article (name,price) values ('28','700000')");
            mydb.execSQL("insert into article (name,price) values ('29','1000000')");
            mydb.execSQL("insert into article (name,price) values ('30','700000')");
            mydb.execSQL("insert into article (name,price) values ('31','1000000')");
            mydb.execSQL("insert into article (name,price) values ('32','700000')");
            mydb.execSQL("insert into article (name,price) values ('33','1000000')");
            mydb.execSQL("insert into article (name,price) values ('34','700000')");
            mydb.execSQL("insert into article (name,price) values ('35','1000000')");
            mydb.execSQL("insert into article (name,price) values ('36','700000')");
            mydb.execSQL("insert into article (name,price) values ('37','1000000')");
            mydb.execSQL("insert into article (name,price) values ('38','700000')");
            mydb.execSQL("insert into article (name,price) values ('39','1000000')");
            mydb.execSQL("insert into article (name,price) values ('40','700000')");



        }
        if (!isTableExists("user", true, mydb)) {
            mydb.execSQL("CREATE TABLE IF NOT EXISTS [user](id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT , password TEXT )");
            prefEditor = login_setting.edit();
            prefEditor.putString("username", "کاربر میهمان");
            prefEditor.putString("id", "-1");
            prefEditor.putBoolean("login_status", false);
            prefEditor.commit();
        }
        else if(login_setting.getBoolean("login_status",false))
        {
            Cursor user_cursor = mydb.rawQuery("SELECT * FROM user", null);
            user_cursor.moveToFirst();
            String username =user_cursor.getString(user_cursor.getColumnIndex("name"));
            String user_id= user_cursor.getString(user_cursor.getColumnIndex("id"));
            prefEditor = login_setting.edit();
            prefEditor.putString("username", username);
            prefEditor.putString("id", user_id);
            prefEditor.putBoolean("login_status", true);
            prefEditor.commit();
            username=login_setting.getString("username","کاربر میهمان");
            user_id=login_setting.getString("id","-1");
        }
        Cursor allrows = mydb.rawQuery("SELECT * FROM article", null);
        ArrayList<String> stringArrayList = new ArrayList<String>();
        while(allrows.moveToNext()){
                String name = allrows.getString(allrows.getColumnIndex("name"));
                stringArrayList.add(name);
        }
        allrows.close();
        mydb.close();
        String[] ArticleList = {};
        ArticleList=stringArrayList.toArray(new String[stringArrayList.size()]);
        ListView article_list = (ListView) findViewById(R.id.new_article);
        CustomListAdapter catListAdapter = new CustomListAdapter(ArticleList, main.this);
        article_list.setAdapter(catListAdapter);
        //animation
        Animation anim_move = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.move);
        article_list.startAnimation(anim_move);
    }
    public boolean isTableExists(String tableName, boolean openDb,SQLiteDatabase mDatabase) {


        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //SharedPreferences
        SharedPreferences login_setting = getSharedPreferences("MyLoginSetting", MODE_PRIVATE);
        if(login_setting.getBoolean("login_status",false))
        {
            getMenuInflater().inflate(R.menu.login_menu, menu);
            MenuItem mi =menu.findItem(R.id.profile);
            mi.setTitle(login_setting.getString("username","کاربر میهمان")+" پروفایل");
        }
        else {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(main.this, signup.class);
            startActivity(intent);
        }
        if (id == R.id.login) {
            Intent intent = new Intent(main.this, LoginActivity.class);
            startActivity(intent);
        }
        if (id == R.id.itemSearch) {
            LinearLayout search_ll=(LinearLayout)findViewById(R.id.search);
            Animation anim_scale = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.scale);
            search_ll.startAnimation(anim_scale);
        }
        return super.onOptionsItemSelected(item);
    }
}
